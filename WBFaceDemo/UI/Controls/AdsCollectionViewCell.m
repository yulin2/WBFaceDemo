//
//  AdsCollectionViewCell.m
//  SenseArDemo
//
//  Created by sluin on 2017/9/7.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "AdsCollectionViewCell.h"
#import "STUtil.h"

@implementation AdsCollectionViewCellModel

@end


@interface AdsCollectionViewCell ()

@end

@implementation AdsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    static UIColor *redColor;
    
    if (!redColor) {
        
        redColor = [UIColor redColor];
    }
    
    [_borderView.layer setBorderColor:redColor.CGColor];
    [_borderView.layer setBorderWidth:2.0f];
    _borderView.layer.cornerRadius = 2.0f;
}


- (void)setModel:(AdsCollectionViewCellModel *)model
{
    _model = model;
    
    if ([NSThread isMainThread]) {
        
        [self refreshUIWithModel:model];
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self refreshUIWithModel:model];
        });
    }
}


- (void)setIsCustomSelected:(BOOL)isCustomSelected
{
    _isCustomSelected = isCustomSelected;
    
    _borderView.hidden = !isCustomSelected;
}

- (void)refreshUIWithModel:(AdsCollectionViewCellModel *)model
{
    static UIColor *blueColor = nil;
    static UIColor *greenColor = nil;
    static UIColor *grayColor = nil;
    
    if (!blueColor) {
        
        blueColor = UIColorFromRGB(0x00abe5);
    }
    
    if (!greenColor) {
        
        greenColor = UIColorFromRGB(0x74c23f);
    }
    
    if (!grayColor) {
        
        grayColor = UIColorFromRGB(0x7d7d80);
    }
    
    
    static NSDateFormatter *formatter = nil;
    
    if (!formatter) {
        
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterShortStyle];
        [formatter setDateFormat:@"HH:mm"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    }
    
    if (!model || !model.material) {
        
        _lblName.hidden = 0 != model.indexOfItem;
        _lblGold.hidden = YES;
        _lblTime.hidden = YES;
        _lblStatus.hidden = YES;
        _lblRemaining.hidden = YES;
        
        _thumbView.hidden = 0 != model.indexOfItem;
        _timeBgView.hidden = YES;
        _loadingView.hidden = YES;
        
        _btnGrab.hidden = YES;
        _btnInfo.hidden = YES;
        _btnDownload.hidden = YES;
        
        if (0 == model.indexOfItem) {
            
            _thumbView.alpha = 1.0f;
            [_lblName setText:[NSString stringWithFormat:@"无贴纸"]];
            _thumbView.image = model.imageThumb;
        }
        
        return;
    }
    
    _thumbView.image = model.imageThumb;
    
    if (CPT_AD == model.material.iMaterialType) {
        
        SenseArCptMaterial *cptMaterial = (SenseArCptMaterial *)model.material;
        
        if (isDownloading == model.downloadStatus
            || model.isGrabbing
            || model.isChecking) {
            
            _thumbView.alpha = 0.5;
        }else{
            
            if (model.isCompleted) {
                
                _thumbView.alpha = 0.5;
            }else{
                
                if (isDownloaded == model.downloadStatus
                    && model.isGrabbed
                    && (SENSEAR_AD_OK == model.iAdStatus || SENSEAR_AD_LEAD_TIME_RANGE == model.iAdStatus)) {
                    
                    _thumbView.alpha = 1.0;
                }else{
                    
                    _thumbView.alpha = 0.5;
                }
            }
        }
        
        
        if (notDownload == model.downloadStatus) {
            
            if (model.isGrabbed && SENSEAR_AD_OK == model.iAdStatus) {
                
                _btnDownload.hidden = NO;
            }else{
                _btnDownload.hidden = YES;
            }
        }else{
            
            _btnDownload.hidden = YES;
        }
        
        _timeBgView.image = [UIImage imageNamed:(SENSEAR_AD_OVER_TIME_RANGE == model.iAdStatus || SENSEAR_AD_OFF_SHELF == model.iAdStatus) ? @"time_bg_01.png" : @"time_bg_02.png"];
        
        NSString *strTimeInfo = [NSString stringWithFormat:@"%@~%@有效",
                                 [formatter stringFromDate:cptMaterial.displayStartDate],
                                 [formatter stringFromDate:cptMaterial.displayEndDate]];
        
        _lblTime.text = strTimeInfo;
        
        // lblStatus
        if (isDownloading == model.downloadStatus
            || model.isGrabbing
            || model.isChecking) {
            
            _lblStatus.hidden = YES;
        }else{
            
            if (model.isCompleted) {
                
                _lblStatus.hidden = NO;
            }else{
                
                if (SENSEAR_AD_OK != model.iAdStatus) {
                    
                    if (model.isGrabbed && SENSEAR_AD_OVER_TIME_RANGE != model.iAdStatus && SENSEAR_AD_OFF_SHELF != model.iAdStatus) {
                        
                        _lblStatus.hidden = YES;
                    }else{
                        
                        _lblStatus.hidden = NO;
                    }
                }else{
                    
                    if (model.isGrabbed) {
                        
                        _lblStatus.hidden = YES;
                    }else{
                        
                        if (SENSEAR_NO_REMAINING == model.iGrabStatus) {
                            
                            _lblStatus.hidden = NO;
                        }else{
                            
                            _lblStatus.hidden = YES;
                        }
                    }
                }
            }
        }
        
        
        if (SENSEAR_AD_LEAD_TIME_RANGE == model.iAdStatus) {
            
            _lblStatus.backgroundColor = blueColor;
            _lblStatus.text = [NSString stringWithFormat:@"%@ 开抢" ,
                               [formatter stringFromDate:cptMaterial.grabStartDate]];
        }
        
        if (SENSEAR_NO_REMAINING == model.iGrabStatus) {
            
            _lblStatus.text = @"已抢完";
            _lblStatus.backgroundColor = grayColor;
        }
        
        if (SENSEAR_AD_OVER_TIME_RANGE == model.iAdStatus) {
            
            _lblStatus.text = @"已过期";
            _lblStatus.backgroundColor = grayColor;
        }
        
        if (SENSEAR_AD_OFF_SHELF == model.iAdStatus) {
            
            _lblStatus.text = @"已下架";
            _lblStatus.backgroundColor = grayColor;
        }
        
        if (model.isCompleted) {
            
            _lblStatus.text = @"已完成";
            _lblStatus.backgroundColor = greenColor;
        }
        
        _lblGold.hidden = NO;
        _lblGold.text = [NSString stringWithFormat:@"%.2f金币" , cptMaterial.fCommissionUnitPrice];
        _lblName.hidden = NO;
        _lblName.text = cptMaterial.strName;
        
        if (model.isGrabbed
            || model.isGrabbing
            || model.isCompleted
            || SENSEAR_CAN_NOT_GRAB == model.iGrabStatus
            || SENSEAR_NO_REMAINING == model.iGrabStatus) {
            
            _btnGrab.hidden = YES;
        }else{
            
            _btnGrab.hidden = NO;
        }
        
        _lblRemaining.hidden = _btnGrab.hidden;
        _lblRemaining.text = [NSString stringWithFormat:@"仅剩%d件" , cptMaterial.iRemainingStock];
        
        if (isDownloading == model.downloadStatus
            || model.isGrabbing
            || model.isChecking) {
            
            _loadingView.hidden = NO;
            [self startDownloadAnimation];
        }else{
            
            _loadingView.hidden = YES;
            [self stopDownloadAnimation];
        }
    }
}


- (void)startDownloadAnimation
{
    [self.loadingView.layer removeAnimationForKey:@"rotation"];
    
    CABasicAnimation *circleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    circleAnimation.duration = 2.5f;
    circleAnimation.repeatCount = MAXFLOAT;
    circleAnimation.toValue = @(M_PI * 2);
    [self.loadingView.layer addAnimation:circleAnimation forKey:@"rotation"];
}

- (void)stopDownloadAnimation
{
    [self.loadingView.layer removeAnimationForKey:@"rotation"];
}

#pragma - mark -
#pragma - mark Button Touch Event


- (IBAction)onBtnInfo:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(btnInfoDidClickInAdsWithWithModel:)]) {
        
        [self.delegate btnInfoDidClickInAdsWithWithModel:self.model];
    }
}

- (IBAction)onBtnDownload:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(btnDownloadDidClickInAdsWithModel:)]) {
        
        [self.delegate btnDownloadDidClickInAdsWithModel:self.model];
    }
}

- (IBAction)onBtnGrab:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(btnGrabDidClickInAdsWithModel:)]) {
        
        [self.delegate btnGrabDidClickInAdsWithModel:self.model];
    }
}

@end
