//
//  FilterCollectionView.m
//  SenseArDemo
//
//  Created by sluin on 2017/9/6.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "FilterCollectionView.h"

@implementation FilterCollectionView


- (instancetype)initWithFrame:(CGRect)frame
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.itemSize = CGSizeMake(100.0, 140.0);
    flowLayout.minimumLineSpacing = 15.0;
    
    self = [super initWithFrame:frame collectionViewLayout:flowLayout];
    
    if (self) {
        
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
    }
    
    return self;
}

@end
