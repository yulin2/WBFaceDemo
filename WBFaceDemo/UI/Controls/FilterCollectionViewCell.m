//
//  FilterCollectionViewCell.m
//  SenseArDemo
//
//  Created by sluin on 2017/9/6.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "FilterCollectionViewCell.h"

@implementation FilterCollectionViewCellModel
@end

@implementation FilterCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.cornerRadius = 10.0f;
    self.layer.borderWidth = 2.0f;
    self.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.thumbView.layer.cornerRadius = 10.0f;
    self.lblName.layer.cornerRadius = 10.0f;
}

- (void)setModel:(FilterCollectionViewCellModel *)model
{
    _model = model;
    
    _thumbView.image = model.image;
    _lblName.text = model.strName;
    
    
    static UIColor *clearColor = nil;
    static UIColor *redColor = nil;
    
    if (!clearColor) {
        
        clearColor = [UIColor clearColor];
    }
    
    if (!redColor) {
        
        redColor = [UIColor redColor];
    }
    
    self.layer.borderColor = model.isSelected ? redColor.CGColor : clearColor.CGColor;
    self.thumbView.alpha = model.isSelected ? 0.5 : 1.0;
}

@end
