//
//  AdsCollectionViewCell.h
//  SenseArDemo
//
//  Created by sluin on 2017/9/7.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SenseArCptMaterial.h"
#import "STEnumDef.h"
#import "EffectsCollectionViewCell.h"


@interface AdsCollectionViewCellModel : EffectsCollectionViewCellModel

@property (nonatomic , assign) int iRemainingStock;

// stauts
@property (nonatomic , assign) SenseArAdStatus iAdStatus;
@property (nonatomic , assign) SenseArCptGrabStatus iGrabStatus;

@property (nonatomic , assign) BOOL isCompleted;
@property (nonatomic , assign) BOOL isGrabbed;
@property (nonatomic , assign) BOOL isGrabbing;
@property (nonatomic , assign) BOOL isChecking;

@end

@protocol AdsCollectionViewCellDelegate <NSObject>

- (void)btnDownloadDidClickInAdsWithModel:(AdsCollectionViewCellModel *)model;
- (void)btnInfoDidClickInAdsWithWithModel:(AdsCollectionViewCellModel *)model;
- (void)btnGrabDidClickInAdsWithModel:(AdsCollectionViewCellModel *)model;

@end


@interface AdsCollectionViewCell : UICollectionViewCell

@property (nonatomic , weak) id <AdsCollectionViewCellDelegate> delegate;

@property (nonatomic , strong) AdsCollectionViewCellModel *model;
@property (nonatomic , assign) BOOL isCustomSelected;


@property (weak, nonatomic) IBOutlet UIImageView *thumbView;

@property (weak, nonatomic) IBOutlet UIImageView *timeBgView;

@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblGold;

@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lblRemaining;

@property (weak, nonatomic) IBOutlet UIButton *btnGrab;

@property (weak, nonatomic) IBOutlet UIButton *btnInfo;

@property (weak, nonatomic) IBOutlet UIButton *btnDownload;

@property (weak, nonatomic) IBOutlet UIImageView *loadingView;

@property (weak, nonatomic) IBOutlet UIView *borderView;


@end
