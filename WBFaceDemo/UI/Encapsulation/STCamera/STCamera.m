//
//  STCamera.m
//
//  Created by sluin on 16/5/4.
//  Copyright © 2016年 SenseTime. All rights reserved.
//

#import "STCamera.h"
#import "STMobileLog.h"
#import <UIKit/UIKit.h>

@interface STCamera () <AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate>

@property (nonatomic , strong) AVCaptureDeviceInput * deviceInput;
@property (nonatomic , strong) AVCaptureVideoDataOutput * dataOutput;
@property (nonatomic , strong) AVCaptureStillImageOutput *stillImageOutput;

@property (nonatomic , strong) AVCaptureSession *session;
@property (nonatomic , strong) AVCaptureDevice *videoDevice;
@property (nonatomic , strong) AVCaptureDevice *audioDevice;

@property (nonatomic , strong) AVCaptureDeviceInput *audioDeviceInput;
@property (nonatomic , strong) AVCaptureAudioDataOutput *audioDataOutput;

@property (nonatomic , readwrite) dispatch_queue_t bufferQueue;

@property (nonatomic , strong , readwrite) AVCaptureConnection *videoConnection;
@property (nonatomic , strong , readwrite) AVCaptureConnection *audioConnection;


@end

@implementation STCamera

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self setupCaptureSession];
    }
    return self;
}

- (void)dealloc
{
    if (self.session) {
        
        self.bSessionPause = YES;
        
        [self.session beginConfiguration];
        
        [self.session removeOutput:self.dataOutput];
        [self.session removeInput:self.deviceInput];
        
        [self.session commitConfiguration];
        
        if ([self.session isRunning]) {
            
            [self.session stopRunning];
        }
        
        self.session = nil;
    }
}

- (void)setupCaptureSession
{
    self.bSessionPause = YES;
    
    dispatch_queue_t bufferQueue = dispatch_queue_create("STCameraBufferQueue", NULL);
    self.bufferQueue = bufferQueue;
    
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    
    self.session = session;
    
    self.audioDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];

    
    NSError *audioError = nil;
    self.audioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.audioDevice error:&audioError];
    if (audioError) {
        NSLog(@"error: %@", audioError.localizedDescription);
    }
    
    self.audioDataOutput = [[AVCaptureAudioDataOutput alloc] init];
    
    [self.audioDataOutput setSampleBufferDelegate:self queue:self.bufferQueue];
    
    [self.session beginConfiguration];
    
    if ([self.session canAddInput:self.audioDeviceInput]) {
        [self.session addInput:self.audioDeviceInput];
    }
    
    if ([self.session canAddOutput:self.audioDataOutput]) {
        [self.session addOutput:self.audioDataOutput];
    }
    
    if ([self.session canSetSessionPreset:AVCaptureSessionPreset1280x720]) {
        
        [self.session setSessionPreset:AVCaptureSessionPreset1280x720];
        
        _sessionPreset = AVCaptureSessionPreset1280x720;
    }
    
    [self.session commitConfiguration];
    
    self.audioConnection = [self.audioDataOutput connectionWithMediaType:AVMediaTypeAudio];
    
    self.audioCompressingSettings = [[self.audioDataOutput recommendedAudioSettingsForAssetWriterWithOutputFileType:AVFileTypeQuickTimeMovie] copy];
    
    self.videoDevice = [self cameraDeviceWithPosition:AVCaptureDevicePositionFront];
    _devicePosition = AVCaptureDevicePositionFront;
    
    NSError *error = nil;
    
    // todo camera auth check
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.videoDevice error:&error];
    
    self.deviceInput = deviceInput;
    
    if (!deviceInput) {
        
        STLog(@"create input error");
        
        return;
    }
    
    AVCaptureVideoDataOutput *dataOutput = [[AVCaptureVideoDataOutput alloc] init];
    
    self.dataOutput = dataOutput;
    
    [dataOutput setAlwaysDiscardsLateVideoFrames:YES];
    
    [dataOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
    
    _bOutputYUV = YES;
    
    [dataOutput setSampleBufferDelegate:self queue:self.bufferQueue];
    
    AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    
    [session beginConfiguration];
    
    if ( [self.session canAddOutput:stillImageOutput] ) {
        
        [self.session addOutput:stillImageOutput];
        self.stillImageOutput = stillImageOutput;
        self.stillImageOutput.outputSettings = @{ AVVideoCodecKey : AVVideoCodecJPEG };
        if ([self.stillImageOutput respondsToSelector:@selector(setHighResolutionStillImageOutputEnabled:)]) {
            
            self.stillImageOutput.highResolutionStillImageOutputEnabled = YES;
        }
    }else {
        
        STLog( @"Could not add still image output to the session" );
    }
    
    if ([session canAddOutput:dataOutput]) {
        
        [session addOutput:dataOutput];
    }else{
        
        STLog( @"Could not add video data output to the session" );
    }
    
    if ([session canAddInput:deviceInput]) {
        
        [session addInput:deviceInput];
    }else{
        
        STLog( @"Could not add device input to the session" );
    }
    
    [self setIFPS:25];
    
    self.videoConnection =  [self.dataOutput connectionWithMediaType:AVMediaTypeVideo];
    if ([self.videoConnection isVideoOrientationSupported]) {
        
        [self.videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
        self.videoOrientation = AVCaptureVideoOrientationPortrait;
    }
    
    if ([self.videoConnection isVideoMirroringSupported]) {
        
        [self.videoConnection setVideoMirrored:YES];
        self.needVideoMirrored = YES;
    }
    
    [session commitConfiguration];
    
    self.videoCompressingSettings = [[_dataOutput recommendedVideoSettingsForAssetWriterWithOutputFileType:AVFileTypeQuickTimeMovie] copy];
}

- (void)setBOutputYUV:(BOOL)bOutputYUV
{
    if (_bOutputYUV != bOutputYUV) {
        
        _bOutputYUV = bOutputYUV;
        
        int iCVPixelFormatType = bOutputYUV ? kCVPixelFormatType_420YpCbCr8BiPlanarFullRange : kCVPixelFormatType_32BGRA;
        
        AVCaptureVideoDataOutput *dataOutput = [[AVCaptureVideoDataOutput alloc] init];
        
        [dataOutput setAlwaysDiscardsLateVideoFrames:YES];
        
        [dataOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:iCVPixelFormatType] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
        
        [dataOutput setSampleBufferDelegate:self queue:_bufferQueue];
        
        _bSessionPause = YES;
        
        [_session beginConfiguration];
        
        [_session removeOutput:_dataOutput];
        
        if ([_session canAddOutput:dataOutput]) {
            
            [_session addOutput:dataOutput];
            _dataOutput = dataOutput;
        }else{
            
            STLog(@"session add data output failed when change output buffer pixel format.");
        }
        
        [_session commitConfiguration];
        
        _videoConnection =  [_dataOutput connectionWithMediaType:AVMediaTypeVideo];
        if ([_videoConnection isVideoOrientationSupported]) {
            
            [_videoConnection setVideoOrientation:_videoOrientation];
        }
        
        if ([_videoConnection isVideoMirroringSupported]) {
            
            [_videoConnection setVideoMirrored:_needVideoMirrored];
        }
        
        _bSessionPause = NO;
    }
}

- (void)setDevicePosition:(AVCaptureDevicePosition)devicePosition
{
    if (_devicePosition != devicePosition && devicePosition != AVCaptureDevicePositionUnspecified) {
        
        if (_session) {
            
            AVCaptureDevice *targetDevice = [self cameraDeviceWithPosition:devicePosition];
            
            if (targetDevice && [self judgeCameraAuthorization]) {
                
                NSError *error = nil;
                AVCaptureDeviceInput *deviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:targetDevice error:&error];
                
                if(!deviceInput || error) {
                    
                    STLog(@"Error creating capture device input: %@", error.localizedDescription);
                    return;
                }
                
                _bSessionPause = YES;
                
                [_session beginConfiguration];
                
                [_session removeInput:_deviceInput];
                
                if ([_session canAddInput:deviceInput]) {
                    
                    [_session addInput:deviceInput];
                    
                    _deviceInput = deviceInput;
                    _videoDevice = targetDevice;
                    
                    _devicePosition = devicePosition;
                }
                
                _videoConnection =  [_dataOutput connectionWithMediaType:AVMediaTypeVideo];
                
                if ([_videoConnection isVideoOrientationSupported]) {
                    
                    [_videoConnection setVideoOrientation:_videoOrientation];
                }
                
                if ([_videoConnection isVideoMirroringSupported]) {
                    
                    [_videoConnection setVideoMirrored:devicePosition == AVCaptureDevicePositionFront];
                    
                }
                                
                [_session commitConfiguration];
                
                _bSessionPause = NO;
            }
        }
    }
}

- (void)setSessionPreset:(NSString *)sessionPreset
{
    if (_session && _sessionPreset) {
        
//        if (![sessionPreset isEqualToString:_sessionPreset]) {
        
        _bSessionPause = YES;
        
        [_session beginConfiguration];
        
        if ([_session canSetSessionPreset:sessionPreset]) {
            
            [_session setSessionPreset:sessionPreset];
            
            _sessionPreset = sessionPreset;
        }
        
        // todo max frame rate.
        if (_iFPS > 0) {
            
            CMTime frameDuration = CMTimeMake(1 , _iFPS);
            
            if ([_videoDevice lockForConfiguration:nil]) {
                
                _videoDevice.activeVideoMaxFrameDuration = frameDuration;
                _videoDevice.activeVideoMinFrameDuration = frameDuration;
                
                [_videoDevice unlockForConfiguration];
            }
        }
        
        [_session commitConfiguration];
        
        self.videoCompressingSettings = [[self.dataOutput recommendedVideoSettingsForAssetWriterWithOutputFileType:AVFileTypeQuickTimeMovie] copy];
        
        _bSessionPause = NO;
//        }
    }
}

- (void)setIFPS:(int)iFPS
{
    // todo max frame rate.
    if (iFPS > 0) {
     
        CMTime frameDuration = CMTimeMake(1 , iFPS);
        
        [_session beginConfiguration];
        
        if ([_videoDevice lockForConfiguration:nil]) {
            
            _videoDevice.activeVideoMaxFrameDuration = frameDuration;
            _videoDevice.activeVideoMinFrameDuration = frameDuration;
            
            [_videoDevice unlockForConfiguration];
        }
        
        [_session commitConfiguration];
        
        _iFPS = iFPS;
    }
}

- (void)startRunning
{
    if (![self judgeCameraAuthorization] && ![self judgeMicAuthorization]) {
        
        return;
    }
    
    if (!self.dataOutput) {
        
        return;
    }
    
    if (self.session && ![self.session isRunning]) {
        
        [self.session startRunning];
        self.bSessionPause = NO;
    }
}


- (void)stopRunning
{
    if (self.session && [self.session isRunning]) {
        
        [self.session stopRunning];
        self.bSessionPause = YES;
    }
}

- (CGRect)getZoomedRectWithRect:(CGRect)rect scaleToFit:(BOOL)bScaleToFit
{
    CGRect rectRet = rect;
    
    if (self.dataOutput.videoSettings) {
        
        CGFloat fWidth = [[self.dataOutput.videoSettings objectForKey:@"Width"] floatValue];
        CGFloat fHeight = [[self.dataOutput.videoSettings objectForKey:@"Height"] floatValue];
        
        float fScaleX = fWidth / CGRectGetWidth(rect);
        float fScaleY = fHeight / CGRectGetHeight(rect);
        float fScale = bScaleToFit ? fmaxf(fScaleX, fScaleY) : fminf(fScaleX, fScaleY);
        
        fWidth /= fScale;
        fHeight /= fScale;
        
        CGFloat fX = rect.origin.x - (fWidth - rect.size.width) / 2.0f;
        CGFloat fY = rect.origin.y - (fHeight - rect.size.height) / 2.0f;
        
        rectRet = CGRectMake(fX, fY, fWidth, fHeight);
    }
    
    return rectRet;
}

- (BOOL)judgeCameraAuthorization
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"请打开相机权限" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        
        [alert show];
        
        return NO;
    }
    
    return YES;
}

- (BOOL)judgeMicAuthorization
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    
    if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"请打开相机权限" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        
        [alert show];
        
        return NO;
    }
    
    return YES;
}

- (AVCaptureDevice *)cameraDeviceWithPosition:(AVCaptureDevicePosition)position
{
    AVCaptureDevice *deviceRet = nil;
    
    if (position != AVCaptureDevicePositionUnspecified) {
        
        NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
        
        for (AVCaptureDevice *device in devices) {
            
            if ([device position] == position) {
                
                deviceRet = device;
            }
        }
    }
    
    return deviceRet;
}

- (AVCaptureVideoPreviewLayer *)previewLayer
{
    if (!_previewLayer) {
        
        _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    }
    
    return _previewLayer;
}

- (void)snapStillImageCompletionHandler:(void (^)(CMSampleBufferRef imageDataSampleBuffer, NSError *error))handler
{
    if ([self judgeCameraAuthorization]) {
        self.bSessionPause = YES;
        
        NSString *strSessionPreset = [self.sessionPreset mutableCopy];
        self.sessionPreset = AVCaptureSessionPresetPhoto;
        
        // 改变preset会黑一下
        [NSThread sleepForTimeInterval:0.3];
        
        dispatch_async(self.bufferQueue, ^{
            
            [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:[self.stillImageOutput connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                
                self.bSessionPause = NO;
                self.sessionPreset = strSessionPreset;
                handler(imageDataSampleBuffer , error);
            }];
        } );
    }
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    if (!self.bSessionPause) {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(captureOutput:didOutputSampleBuffer:fromConnection:)]) {
            //[connection setVideoOrientation:AVCaptureVideoOrientationPortrait];
            [self.delegate captureOutput:captureOutput didOutputSampleBuffer:sampleBuffer fromConnection:connection];
        }
    }
}

@end
