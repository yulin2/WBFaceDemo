//
//  SenseArMemoryCache.h
//  SenseAr
//
//  Created by sluin on 2017/5/9.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SenseArMemoryCache : NSObject

@property (readonly, copy) NSArray<id> *allKeys;
@property (readonly, copy) NSArray<id> *allValues;

- (void)setObject:(id)anObject forKey:(id)aKey;
- (id)objectForKey:(id)aKey;
- (void)removeObjectForKey:(id)aKey;
- (void)removeAllObjects;

@end
