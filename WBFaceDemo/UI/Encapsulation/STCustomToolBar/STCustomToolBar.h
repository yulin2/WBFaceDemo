//
//  STCustomToolBar.h
//  SenseArDemo
//
//  Created by sluin on 2017/8/28.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    
    STCustomToolBarDottedMark = 0,
    STCustomToolBarSquareMark
    
} STCustomToolBarMarkViewStyle;

@interface STCustomToolBarItem : NSObject

@property (nonatomic , copy) NSString *strTitle;
@property (nonatomic , strong) UIImage *defaultImage;
@property (nonatomic , strong) UIImage *selecedImage;

+ (instancetype)itemWithTitle:(NSString *)strTitle
                 defaultImage:(UIImage *)defaultImage
                 selecedImage:(UIImage *)selectedImage;

STCustomToolBarItem *createCustomToolBarItem(NSString *strTitle ,
                                             UIImage *defaultImage ,
                                             UIImage *selectedImage);

@end

@interface STCustomToolBar : UIView

@property (nonatomic , assign) int iSelectedIndex;

- (instancetype)initWithFrame:(CGRect)frame
                scrollEnabled:(BOOL)scrollEnabled
                   equalParts:(BOOL)isEqualParts
        firstItemLeadingSpace:(int)iFirstItemLeadingSpace
                      spacing:(int)iSpacing
           highlightedColor:(UIColor *)highlightedColor
                markViewStyle:(STCustomToolBarMarkViewStyle)markViewStyle
                        items:(NSArray <STCustomToolBarItem *>*)arrItems
               selectCallback:(void(^)(int iSelectedIndex))callback;

@end
