//
//  SenseArMaterialHistoryInfo.h
//  SenseAr
//
//  Created by sluin on 2017/5/4.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SenseArMaterialHistoryInfo : NSObject <NSCoding>


/**
 素材ID
 */
@property (nonatomic , copy , readonly) NSString *strID;


/**
 素材名称
 */
@property (nonatomic , copy , readonly) NSString *strName;


/**
 素材缩略图地址
 */
@property (nonatomic , copy , readonly) NSString *strThumbnailURL;


/**
 媒体分成单价
 */
@property (nonatomic , assign , readonly) float fCommissionUnitPrice;


/**
 广告任务完成的日期
 */
@property (nonatomic , strong , readonly) NSDate *completionDate;





@end
