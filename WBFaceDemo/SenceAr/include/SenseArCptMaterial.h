//
//  SenseArCptMaterial.h
//  SenseAr
//
//  Created by sluin on 2017/4/26.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "SenseArMaterial.h"

// 广告状态
typedef enum : NSUInteger {
    
    // 未开始 (不在广告有效展示时间内,时间超前)
    SENSEAR_AD_LEAD_TIME_RANGE = 0,
    
    // 有效
    SENSEAR_AD_OK,
    
    // 过期 (不在广告有效展示时间内,时间落后)
    SENSEAR_AD_OVER_TIME_RANGE,
    
    // 下架
    SENSEAR_AD_OFF_SHELF
    
} SenseArAdStatus;

// 广告的可抢状态
typedef enum : NSUInteger {
    
    // 不可抢
    SENSEAR_CAN_NOT_GRAB = 0,
    
    // 可以抢
    SENSEAR_CAN_GRAB,
    
    // 已抢完
    SENSEAR_NO_REMAINING
    
} SenseArCptGrabStatus;


// 广告抢单结果
typedef enum : NSUInteger {
    
    // 抢单成功
    SENSEAR_GRAB_SUCCESS = 0,
    
    // 未开始 (不在服务端可抢范围内 , 时间超前)
    SENSEAR_GRAB_LEAD_TIME_RANGE,
    
    // 已过期 (不在服务端可抢范围内 , 时间落后)
    SENSEAR_GRAB_OVER_TIME_RANGE,
    
    // 已抢完 (库存为 0)
    SENSEAR_GRAB_NO_REMAINING,
    
    // 已下架
    SENSEAR_GRAB_OFF_SHELF
    
} SenseArCptGrabResult;


// 广告完成确认结果
typedef enum : NSUInteger {
    
    // 确认成功
    SENSEAR_CONFIRM_SUCCESS = 0,
    
    // 已过期
    SENSEAR_CONFIRM_OVER_TIME_RANGE,
    
    // 重复确认
    SENSEAR_CONFIRM_REPEATED,
    
    // 已下架
    SENSEAR_CONFIRM_OFF_SHELF
    
} SenseArCptConfirmResult;



@interface SenseArCptMaterial : SenseArMaterial <NSCoding>


/**
 广告链接
 */
@property (nonatomic , copy , readonly) NSString *strAdLink;


/**
 广告语
 */
@property (nonatomic , copy , readonly) NSString *strAdSlogan;


/**
 最小广告展示时间 单位:秒
 */
@property (nonatomic , assign , readonly) int iMinDisplayTime;


/**
 广告有效展示起始时间
 */
@property (nonatomic , strong , readonly) NSDate *displayStartDate;


/**
 广告有效展示截止时间
 */
@property (nonatomic , strong , readonly) NSDate *displayEndDate;


/**
 媒体分成单价
 */
@property (nonatomic , assign , readonly) float fCommissionUnitPrice;


/**
 广告状态
 */
@property (nonatomic , assign , readonly) SenseArAdStatus iAdStatus;


/**
 广告的可抢状态
 */
@property (nonatomic , assign , readonly) SenseArCptGrabStatus iGrabStatus;


/**
 剩余库存
 */
@property (nonatomic , assign , readonly) int iRemainingStock;


/**
 广告可抢单起始时间
 */
@property (nonatomic , strong , readonly) NSDate *grabStartDate;


/**
 广告可抢单截止时间
 */
@property (nonatomic , strong , readonly) NSDate *grabEndDate;


/**
 广告抢单状态 , YES : 已抢到 , NO : 未抢到
 */
@property (nonatomic , assign , readonly) BOOL isGrabbed;


/**
 广告完成状态 , YES : 已完成 , NO : 未完成
 */
@property (nonatomic , assign , readonly) BOOL isCompleted;



@end
