//
//  SenseArAudienceClient.h
//  SenseArMaterial
//
//  Created by sluin on 16/10/8.
//  Copyright © 2016年 SenseTime. All rights reserved.
//

#import "SenseArClient.h"
#import <GLKit/GLKit.h>

/**
 粉丝类型客户
 */
@interface SenseArAudienceClient : SenseArClient


/**
 *  是否正在观看直播
 */
@property (nonatomic , assign , readonly) BOOL isWatching;


/**
 开始观看直播

 @param strBroadcasterID 平台上的主播唯一标识
 */
- (void)watchBroadcastStart:(NSString *)strBroadcasterID;

/**
 停止观看直播
 */
- (void)watchBroadcastEnd;


@end
