//
//  WBCameraEffectCache.m
//  WBCameraEffectCache
//
//  Created by hengjing on 2018/6/25.
//  Copyright © 2018年 weibo. All rights reserved.
//

#import "WBCameraEffectCache.h"

#import "AFHTTPSessionManager.h"
#import "SSZipArchive.h"

#import "NSString+WBTUtilities.h"

static NSString *const kErrorDomain = @"wb_camera_effectt_cache_error_domain";

#define kCacheFolderPath [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:[@"WBCameraEffectCache" wbt_stringWithMD5]]

@interface WBCameraEffectCache ()
{
    
}
@end

@implementation WBCameraEffectCache

static NSError *makeError(unsigned long errCode, NSString* errMsg) {
    return [NSError errorWithDomain:kErrorDomain
                               code:errCode
                           userInfo:@{NSLocalizedDescriptionKey: errMsg}];
}

+ (void)initialize {
    [super initialize];
    
    BOOL isDir = NO;
    if ([[NSFileManager defaultManager] fileExistsAtPath:kCacheFolderPath isDirectory:&isDir]) {
        if (isDir) return;
    }
    
    NSError *error = nil;
    if (![[NSFileManager defaultManager] createDirectoryAtPath:kCacheFolderPath withIntermediateDirectories:YES attributes:nil error:&error]) {
        NSAssert1(NO, @"WBCameraEffectCache create cache folder failed: %@", error);
    }
}

+ (cancelBlock)fetchGroupListSuccess:(void (^)(NSArray<WBCameraEffectGroup *> *))success failure:(void (^)(NSError *))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSURLSessionDataTask *task = [manager GET:@"http://i.multimedia.api.weibo.com/2/multimedia/app_resource/all_labels.json?access_token=2.00LzxKxCCPl5PDf4dce4e2e9TeMZIB" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *error = nil;
        NSArray *temp = [WBCameraEffectGroup arrayOfModelsFromDictionaries:responseObject error:&error];
        if (!error) {
            BLOCK_EXEC(success, temp);
            return;
        }
        
        BLOCK_EXEC(failure, error);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        BLOCK_EXEC(failure, error)
    }];
    
    [task resume];
    
    cancelBlock block = ^{
        [task cancel];
    };
    
    return block;
}

+ (cancelBlock)fetchMaterialList:(NSString *)groupID success:(void (^)(WBCameraEffectMaterialList *))success failure:(void (^)(NSError *))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString * url = [NSString stringWithFormat:@"http://i.multimedia.api.weibo.com/2/multimedia/app_resource/get_resources_by_label.json?access_token=2.00LzxKxCU31M2D8122b65a6dFDyrME&label_id=%@&page=1&count=21",groupID];
    NSURLSessionDataTask *task = [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *error = nil;
        WBCameraEffectMaterialList *list = [[WBCameraEffectMaterialList alloc] initWithDictionary:responseObject error:&error];
        if (!error) {
            BLOCK_EXEC(success, list)
            return;
        }
        
        BLOCK_EXEC(failure, error)
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        BLOCK_EXEC(failure, error)
    }];
    
    [task resume];
    
    cancelBlock block = ^{
        [task cancel];
    };
    
    return block;
}

+ (NSString *)materialLocalPath:(NSString *)materialUrl {
    NSString *materialFileName = [materialUrl wbt_stringWithMD5];
    NSString *materialFolderPath = [kCacheFolderPath stringByAppendingPathComponent:materialFileName];
    BOOL isDir = NO;
    if ([[NSFileManager defaultManager] fileExistsAtPath:materialFolderPath isDirectory:&isDir]) {
        return materialFolderPath;
    }
    
    return nil;
}

+ (void)unzipFile:(NSString *)path destination:(NSString *)destination complete:(void (^)(BOOL success))complete {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        BOOL result = [SSZipArchive unzipFileAtPath:path toDestination:destination];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            BLOCK_EXEC(complete, result);
            
            [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
        });
    });
}

+ (cancelBlock)materialWithUrl:(NSString *)materialUrl progress:(void (^)(CGFloat progress))progress completionHandler:(void (^)(NSString *materialUrl, NSString *localPath, NSError *error))completionHandler {
    
    NSString *materialLocalPath = [self materialLocalPath:materialUrl];
    if (materialLocalPath && materialLocalPath.length) {
        BLOCK_EXEC(completionHandler, materialUrl, materialLocalPath, nil)
        return NULL;
    }
    
    NSString *materialFileName = [materialUrl wbt_stringWithMD5];
    NSString *materialFolderPath = [kCacheFolderPath stringByAppendingPathComponent:materialFileName];
    NSURL *destURL = [NSURL fileURLWithPath:[materialFolderPath stringByAppendingString:@"_temp"]];
    if (!destURL) {
        BLOCK_EXEC(completionHandler, materialUrl, nil, nil)
        return NULL;
    }
    
    dispatch_block_t unzipBlock = ^{
        NSError *error = nil;
        if ([[NSFileManager defaultManager] createDirectoryAtPath:materialFolderPath withIntermediateDirectories:YES attributes:nil error:&error]) {
            
            [self unzipFile:destURL.path destination:materialFolderPath complete:^(BOOL success) {
                BLOCK_EXEC(completionHandler, materialUrl, success ? materialFolderPath : nil,  !success ?makeError(WBCameraEffectCacheErrorUnzipMaterialFailed, @"unzip material failed") : nil)
            }];
        } else {
            BLOCK_EXEC(completionHandler, materialUrl, nil, makeError(WBCameraEffectCacheErrorCreateMaterialFolderFailed, @"create material folder failed!"))
        }
    };
    
    BOOL isDir = NO;
    if ([[NSFileManager defaultManager] fileExistsAtPath:destURL.path isDirectory:&isDir]) {
        if (!isDir) {
            unzipBlock();
            return NULL;
        }
    }
    
    [[NSFileManager defaultManager] removeItemAtURL:destURL error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:materialUrl]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSURLSessionDownloadTask *task = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        if (progress) {
            progress(downloadProgress.fractionCompleted);
        }
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return destURL;
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (error) {
            BLOCK_EXEC(completionHandler, materialUrl, nil, error)
        } else {
            unzipBlock();
        }
    }];
    
    [task resume];
    
    cancelBlock block = ^{
        [task cancel];
    };
    
    return block;
}

+ (cancelBlock)materialWithUrl:(NSString *)materialUrl completionHandler:(void (^)(NSString *materialUrl, NSString *localPath, NSError *error))completionHandler {
    return [self materialWithUrl:materialUrl progress:nil completionHandler:completionHandler];
}

@end
