//
//  NSString+WBTUtilities.m
//  WBCameraEffectCache
//
//  Created by hengjing on 2018/6/27.
//  Copyright © 2018年 weibo. All rights reserved.
//

#import "NSString+WBTUtilities.h"

#include <CommonCrypto/CommonDigest.h>

@implementation NSString (WBTUtilities)

- (NSString *)wbt_stringWithMD5 {
    unsigned char hashedChars[CC_MD5_DIGEST_LENGTH];
    
    const char *cData = [self cStringUsingEncoding:NSUTF8StringEncoding];
    
    CC_MD5(cData, (CC_LONG)strlen(cData), hashedChars);
    
    char hash[2 * sizeof(hashedChars) + 1];
    for (size_t i = 0; i < sizeof(hashedChars); ++i) {
        snprintf(hash + (2 * i), 3, "%02x", (int)(hashedChars[i]));
    }
    
    return [NSString stringWithUTF8String:hash];
}

@end
