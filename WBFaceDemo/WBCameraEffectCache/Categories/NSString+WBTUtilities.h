//
//  NSString+WBTUtilities.h
//  WBCameraEffectCache
//
//  Created by hengjing on 2018/6/27.
//  Copyright © 2018年 weibo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (WBTUtilities)

- (NSString *)wbt_stringWithMD5;

@end
