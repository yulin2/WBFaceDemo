//
//  WBCameraEffectCache.h
//  WBCameraEffectCache
//
//  Created by hengjing on 2018/6/25.
//  Copyright © 2018年 weibo. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WBCameraEffectMaterialList.h"
#import "WBCameraEffectGroup.h"

typedef NS_ENUM(NSUInteger, WBCameraEffectCacheError) {
    WBCameraEffectCacheErrorCreateMaterialFolderFailed = 1000,
    WBCameraEffectCacheErrorUnzipMaterialFailed,
};

typedef void(^cancelBlock)(void);

@interface WBCameraEffectCache : NSObject

+ (cancelBlock)fetchGroupListSuccess:(void (^)(NSArray<WBCameraEffectGroup *> *))success failure:(void (^)(NSError *))failure;

+ (cancelBlock)fetchMaterialList:(NSString *)groupID success:(void (^)(WBCameraEffectMaterialList *))success failure:(void (^)(NSError *))failure;

+ (NSString *)materialLocalPath:(NSString *)materialUrl;

+ (cancelBlock)materialWithUrl:(NSString *)materialUrl progress:(void (^)(CGFloat progress))progress completionHandler:(void (^)(NSString *materialUrl, NSString *localPath, NSError *error))completionHandler;

+ (cancelBlock)materialWithUrl:(NSString *)materialUrl completionHandler:(void (^)(NSString *materialUrl, NSString *localPath, NSError *error))completionHandler;

@end
