//
//  WBCameraEffectGroup.h
//  WBCameraEffectCache
//
//  Created by hengjing on 2018/6/26.
//  Copyright © 2018年 weibo. All rights reserved.
//

#import "JSONModel.h"

@interface WBCameraEffectGroup : JSONModel
/** 分组ID，⾮空 */
@property (nonatomic, copy) NSString *label_id;
/** 分组名称，⾮非空 */
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString<Optional> *strIcon;
/** 扩展字段JSON，可空 */
@property (nonatomic, strong) NSDictionary<Optional> *ext_info;

@end
