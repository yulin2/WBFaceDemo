//
//  WBCameraEffectMaterial.h
//  WBCameraEffectCache
//
//  Created by hengjing on 2018/6/26.
//  Copyright © 2018年 weibo. All rights reserved.
//

#import "JSONModel.h"

@interface WBCameraEffectMaterial : JSONModel
/** 素材⽂文件ID */
@property (nonatomic, copy) NSString *app_resource_id;
/** 素材名称 (人脸贴纸) */
@property (nonatomic, copy) NSString *name;
/** 素材描述（人脸基础贴纸） */
@property (nonatomic, copy) NSString *desc;
/** 素材类型（0:2d素材，1:⼈人脸素材，2:3d素材） */
@property (nonatomic, assign) NSInteger resource_type;
/** 素材缩略略图地址 */
@property (nonatomic, copy) NSString *thumbnail_url;
/** 素材⽂文件地址 */
@property (nonatomic, copy) NSString *file_url;
/** 扩展字段 */
@property (nonatomic, strong) NSDictionary<Optional> *ext_info;
/** 版本信息 */
@property (nonatomic, copy) NSString<Optional> *version;
/** 针对strMeterialURL⽂文件的md5值 */
@property (nonatomic, copy) NSString *md5;
@end

@interface materialAction :JSONModel

@property (nonatomic, assign) NSInteger iTriggerAction;

@property (nonatomic, copy) NSString * strTriggerActionTip;

@end
