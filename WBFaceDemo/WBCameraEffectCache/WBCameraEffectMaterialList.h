//
//  WBCameraEffectMaterialList.h
//  WBCameraEffectCache
//
//  Created by hengjing on 2018/7/17.
//  Copyright © 2018年 weibo. All rights reserved.
//

#import "JSONModel.h"

#import "WBCameraEffectMaterial.h"

NS_ASSUME_NONNULL_BEGIN

@protocol WBCameraEffectMaterial @end;

@interface WBCameraEffectMaterialList : JSONModel

@property (nonatomic, strong) NSArray<WBCameraEffectMaterial> *list;
/** 素材总数 */
@property (nonatomic, assign) NSInteger total;
/** 当前页码 */
@property (nonatomic, assign) NSInteger page;
/** 每页大小 */
@property (nonatomic, assign) NSInteger count;

@end

NS_ASSUME_NONNULL_END
